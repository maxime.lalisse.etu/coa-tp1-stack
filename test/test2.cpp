// This is not the main
#include "catch.hpp"
#include "stack.h"

// Basic test
TEST_CASE("Create a Stack and insert an element", "[stack]")
{
    Stack s;
    s.push(1);
    REQUIRE(s.size() == 1);
    s.pop();
    REQUIRE(s.size() == 0);
    REQUIRE(s.isEmpty() == true);
}

// Test that an exception is raised when we try to read from an empty
// stack
TEST_CASE("Pop an element from an empty stack", "[stack]")
{
    Stack s;
    s.push(1);
    REQUIRE(s.size() == 1);
    s.pop();    

    REQUIRE_THROWS(s.top());
}

// Test that the element just inserted is always at the top
TEST_CASE("Push and read top", "[stack]")
{
    Stack s;

    for (int i=0; i<1000; i++) {
        s.push(i);
        REQUIRE(s.top() == i);
    }   
}

// Elements are extracted in inverse order or insertion
TEST_CASE("Inverted read", "[stack]")
{
    Stack s;

    for (int i = 0; i<1000; i++)
        s.push(i);

    for (int i=999; i>=0; i--) {
        REQUIRE(s.top() == i);
        s.pop();
    }
}

// Test that the size is always equal to the number of elements inside
TEST_CASE("Size", "[stack]")
{
    Stack s;

    for (int i=0; i<1000; i++) {
        s.push(i);
        REQUIRE(s.size() == i+1);
    }
}


// Test that maxsize is always greater than size
TEST_CASE("MaxSize", "[stack]")
{
    Stack s;

    for (int i=0; i<1000; i++) {
        s.push(i);
        REQUIRE(s.size() <= s.maxsize());
    }

    for (int i=999; i>=0; i--) {
        s.pop();
        REQUIRE(s.size() <= s.maxsize());
        REQUIRE(i <= s.size());
    }
}

