#include "stack.h"

#include <iostream>

using namespace std;

int main()
{
    Stack s;

    s.push(9);
    cout << s.isEmpty() << endl;
    cout << s.top() << endl;

    Stack s2{s};

    cout << s << endl;
    cout << s2 << endl;

    cout << ((s == s2) ? "egal" : "different") << endl;
    s2.pop();
    cout << ((s == s2) ? "egal" : "different") << endl;
    s2.push(9);
    cout << ((s == s2) ? "egal" : "different") << endl;

    s.push(1);
    s.push(2);
    s.push(3);

    cout << s << endl;
}

