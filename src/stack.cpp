#include <stack.h>
#include <iostream>

Stack::Stack()
{
  this->s = new int[capacity];
}

Stack::Stack(const Stack &other)
{
  this->capacity = other.capacity;
  this->s = new int[this->capacity];
  this->n = other.n;
  for (int i = 0; i < this->n; i++) {
    this->s[i] = other.s[i];
  }
}

Stack::~Stack()
{
  delete[] this->s;
}

bool Stack::isEmpty() const
{
    return this->n == 0;
}

int Stack::top() const
{
  if (this->n > 0) {
    return this->s[this->n - 1];
  } else {
    throw EmptyExc{};
  }

  return 0;
}

void Stack::pop()
{
  if (this->n > 0) {
    this->n--;
  }
}

void Stack::push(int elem)
{
  if (this->n >= this->capacity - 1) {
    this->capacity = this->capacity * 2;
    int *s2 = new int[this->capacity];
    for (int i = 0; i < this->n; i++) {
      s2[i] = this->s[i];
    }
    delete[] this->s;
    this->s = s2;
  }

  this->s[this->n++] = elem;
}

void Stack::clear()
{
  this->n = 0;
}

int Stack::size() const
{
  return this->n;
}

int Stack::maxsize() const
{
    return this->capacity;
}

std::ostream& operator<<(std::ostream &os, const Stack &s) {
  os << "[";
  for (int i = 0; i < s.n; i++) {
    os << s.s[i];
    if (i < s.n - 1) {
      os << ", ";
    }
  }
  os << "]";
  return os;
}

bool Stack::operator==(const Stack &other) const {
  if (this->n == other.n) {
    for (int i = 0; i < this->n; i++) {
      if (this->s[i] != other.s[i]) {
        return false;
      }
    }
    return true;
  }
  return false;
}
